#!/bin/bash

docker build -t solution .

xhost +local:docker &> /dev/null
DOCKER_COMMON_ARGS="--env=DISPLAY --env=QT_X11_NO_MITSHM=1 --volume=/tmp/.X11-unix:/tmp/.X11-unix"

docker run -it --rm --net=host -v /dev/shm:/dev/shm --device=/dev/dri:/dev/dri \
--privileged $DOCKER_COMMON_ARGS --name solution \
--rm solution

