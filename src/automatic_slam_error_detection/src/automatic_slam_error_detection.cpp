#include "automatic_slam_error_detection/automatic_slam_error_detection.h"
  
void AutomaticSlamErrorDetector::slam_error_detector_callback(const visualization_msgs::MarkerArray& slam_confidence_marker)
  {  
    std_msgs::Bool slam_error_signal;
    slam_error_signal.data = false;
    std::string confidence_indicators_text{slam_confidence_marker.markers[1].text};
    std::string delimiter{"|"};
    size_t pos = 0;
    std::string single_token{""};
    std::vector<float> tokens;
    while ((pos = confidence_indicators_text.find(delimiter)) != std::string::npos) {
      single_token = confidence_indicators_text.substr(0, pos);
      tokens.push_back(std::stof(single_token));
      confidence_indicators_text.erase(0, pos + delimiter.length());
    }

    if(tokens[0]<40)
    {
      slam_error_signal.data = true;
      ROS_ERROR("Localization failed: The confidence level is %f, which lies below threshold of 40", tokens[0]);
    }
    else
    {
      ROS_INFO("Confidence level of localization: %f", tokens[0]);
      
    }
    pub.publish(slam_error_signal);    
  }

