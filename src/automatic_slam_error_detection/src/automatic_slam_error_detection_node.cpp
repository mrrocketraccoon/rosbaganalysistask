#include "automatic_slam_error_detection/automatic_slam_error_detection.h"

int main(int argc, char * argv[])
{
  ros::init(argc, argv, "automatic_slam_error_detection");
  AutomaticSlamErrorDetector SlamErrorDetector;
  ros::spin();
  return 0;
}
