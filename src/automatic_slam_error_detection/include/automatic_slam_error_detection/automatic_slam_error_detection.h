#pragma once
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "visualization_msgs/MarkerArray.h"
#include <string>
class AutomaticSlamErrorDetector
{
private:
  ros::NodeHandle n; 
  ros::Publisher pub;
  ros::Subscriber sub;

public:
  AutomaticSlamErrorDetector()
  {
    pub = n.advertise<std_msgs::Bool>("slam_error_signal", 10);
    sub = n.subscribe("/lts_confidence_marker", 1, &AutomaticSlamErrorDetector::slam_error_detector_callback, this);
  }
  void  slam_error_detector_callback(const visualization_msgs::MarkerArray& new_goal);
};//End of class SubscribeAndPublish
