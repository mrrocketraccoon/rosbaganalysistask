FROM osrf/ros:melodic-desktop-full

ARG REPO_WS=/rosbag_analysis_task
RUN mkdir -p $REPO_WS
WORKDIR /home/user/$REPO_WS
COPY ./ /home/user/$REPO_WS
RUN bash -c "source /opt/ros/melodic/setup.bash \
    && catkin_make \
    && echo 'source /opt/ros/melodic/setup.bash' >> ~/.bashrc \
    && source devel/setup.bash \
    && echo 'source devel/setup.bash' >> ~/.bashrc \ 
    && echo 'roslaunch automatic_slam_error_detection automatic_slam_error_detection.launch' >> ~/.bashrc"
ENTRYPOINT ["bash"]
