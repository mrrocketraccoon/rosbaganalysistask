# RosbagAnalysisTask



## Task 1: Error Analyzing
By examining the rosbag recorded at 11:28 we can appreciate that the robot finds itself in a situation where the localization algorithm fails. If this is the first time that this behavior occurs, it would probably be due to considerable changes in the working environment. Maybe some obstacles were placed in the working area that made the environment unrecognizable for the localization algorithm. The customer would be adviced to remove the obstacles that interfere with the localization, or remap the environment so that the algorithm is able to recognize and differentiate this "updated section" of the environment. If this behavior happens often at this section, then the customer would be advised to make this section of the environment easier, to recognize, and differentiate from other potentially similar sections, by adding obstacles that do not interfere with the robot's operation.
If removing or adding obstacles is not feasible, one possible solution that could be proposed to the customer would be to place artificial landmarks in the environment and modify the robot's localization algorithm to incorporate the new information. This gain of information would make the algorithm more robust to changes in the environment.

The following animation shows the localization error. We are able to appreciate that as the robot moves upwards to its goal, at some point the particle cloud that describes the position of the robot becomes very sparse. We also appreciate that the /lts_confidence_marker topic provides us with information about the confidence of the localization algorithm. We see that at the beginning the arrows that represent this confidence are green (high confidence) and as time progresses the arrows become red (low confidence).

[localization_error]: ./media/localization_error.gif "Visualization of localization error"
![alt text][localization_error]

 We can build upon the information provided by the /lts_confidence_marker topic to come up with a solution for the automated error detection. An alternative approach would be to average the confidence of the particles provided by the particle filter and put a threshold the result to detect low and high confidence values.


## Task 2: Automatic Error Detection

### Explanation 

As briefly presented before, the analysis pointed to two good candidate solutions to detect the localization error automatically. I have opted for building upon the information provided by the /lts_confidence_marker topic. This not only makes the coding slightly easier. It also helps me to leverage on the previous results obtained by the developers that already worked on the system. By experimenation, the confidence threshold was set to 40. Whenever the confidence value fails to reach this threshold, the program considers the actual state of the robot a localization error, it publishes a value of true to the boolean topic /slam_error_signal and displays an error message in the terminal.
Whenever the confidence value surpasses the threshold, the value will be displayed in the terminal as an information message.

### Running the error detection node locally
In order to run the detection node locally you can use the following commands:

```
git clone https://gitlab.com/mrrocketraccoon/rosbaganalysistask.git
cd ~/rosbaganalysistask
catkin_make
roslaunch automatic_slam_error_detection automatic_slam_error_detection.launch
```

### Running the error detection node using the docker container

- To satisfy the requirement of capability of using the tool on another machine, a docker container is provided. The container can be started using the bash script provided. It is assumed that you have a working installation of the docker engine.

```
sudo bash start.sh
```

